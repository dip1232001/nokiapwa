import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { LeftmenuComponent } from './leftmenu/leftmenu.component';
import { HomeComponent } from './home/home.component';
import { NavigationComponent } from './navigation/navigation.component';
import { GlobalContentComponent } from './global-content/global-content.component';
import { ImplementationComponent } from './implementation/implementation.component';
import { ImplementationServicesComponent } from './implementation-services/implementation-services.component';
import { EnergySoutionsComponent } from './energy-soutions/energy-soutions.component';
const appRoutes: Routes = [
    { path: '', component: HomeComponent, pathMatch: 'full' },
    { path: 'global', component: GlobalContentComponent, pathMatch: 'full' },
    { path: 'implimentation', component: ImplementationComponent, pathMatch: 'full' },
    { path: 'implementation-services', component: ImplementationServicesComponent, pathMatch: 'full' },
    { path: 'energy-solution', component: EnergySoutionsComponent, pathMatch: 'full' },
    { path: '**', redirectTo: '' }
];
@NgModule({
    declarations: [
        AppComponent,
        LeftmenuComponent,
        HomeComponent,
        NavigationComponent,
        GlobalContentComponent,
        ImplementationComponent,
        ImplementationServicesComponent,
        EnergySoutionsComponent
    ],
    imports: [
        BrowserModule,
        RouterModule.forRoot(appRoutes)
    ],

    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
