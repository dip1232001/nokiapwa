import {Component, OnInit} from '@angular/core';
declare var jQuery: any;
@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    constructor() {}

    ngOnInit() {

        jQuery(function ($) {
            if ($(window).width() <= 767) {
                $("div.image-block").find('img').each(function () {
                    $(this).attr("src", $(this).attr("src").replace("img/", "img/mobile/"));
                });
            }
        });

    }

}
