import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImplementationServicesComponent } from './implementation-services.component';

describe('ImplementationServicesComponent', () => {
  let component: ImplementationServicesComponent;
  let fixture: ComponentFixture<ImplementationServicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImplementationServicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImplementationServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
