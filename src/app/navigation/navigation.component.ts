import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, NavigationEnd } from '@angular/router';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
declare var jQuery: any;
@Component({
    selector: 'app-navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
    public _currentLocation = '';
    constructor(private _location: Location, private _router: Router) {
        this._router.events
            .filter((event) => event instanceof NavigationEnd)
            .subscribe((event) => {
                this._currentLocation = window.location.pathname;

            });

    }
    backClicked() {
        this._location.back();
    }
    ngOnInit() {

        const isMobile = {
            Android: function () {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function () {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function () {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function () {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function () {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function () {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };
        jQuery(function ($) {
            if (!isMobile.any()) {
                $('a.back').hide();
            }
            $(".collapsible,.back").on("click", function (e) {
                e.preventDefault();
            });
            $("#menu-toggle").click(function (e) {
                e.preventDefault();
                jQuery("#wrapper").addClass("active");
            });

            $('body').click(function (e) {
                var target = $(e.target);
                if (!target.is('#sidebar-wrapper') && !target.is('#menu-toggle') && !target.is('.collapsible') && target.parents('#menu-toggle').length == 0) {
                    e.preventDefault();
                    $("#wrapper").removeClass("active");
                }
            });
        });
    }

}
