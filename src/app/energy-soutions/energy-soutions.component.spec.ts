import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnergySoutionsComponent } from './energy-soutions.component';

describe('EnergySoutionsComponent', () => {
  let component: EnergySoutionsComponent;
  let fixture: ComponentFixture<EnergySoutionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnergySoutionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnergySoutionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
