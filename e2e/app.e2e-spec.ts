import { NokiaHtmlPage } from './app.po';

describe('nokia-html App', () => {
  let page: NokiaHtmlPage;

  beforeEach(() => {
    page = new NokiaHtmlPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
