module.exports = {
  navigateFallback: '/index.html',
  stripPrefix: 'dist',
  root: 'dist/',
  staticFileGlobs: [
    'dist/**/*.html',
    'dist/**/*.js',
    'dist/**/*.css',
    'dist/**/*.png',
    'dist/**/*.jpeg',
    'dist/**/*.jpg',
    'dist/**/*.gif'
  ]
};